package com.mintic.seguridad04.Repositorios;

import com.mintic.seguridad04.Modelos.Permiso;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RepositorioPermiso extends MongoRepository<Permiso, String> {
}
