package com.mintic.seguridad04.Repositorios;

import com.mintic.seguridad04.Modelos.Rol;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RepositorioRol extends MongoRepository<Rol, String> {
}
