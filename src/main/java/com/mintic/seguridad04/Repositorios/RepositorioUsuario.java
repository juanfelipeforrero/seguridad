package com.mintic.seguridad04.Repositorios;

import com.mintic.seguridad04.Modelos.Usuario;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RepositorioUsuario extends MongoRepository<Usuario, String> {
}
