package com.mintic.seguridad04;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Seguridad04Application {

	public static void main(String[] args) {
		SpringApplication.run(Seguridad04Application.class, args);
	}

}
